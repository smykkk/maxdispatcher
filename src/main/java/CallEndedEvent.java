import interfaces.ICallListener;
import interfaces.IEvent;

import java.util.List;

public class CallEndedEvent implements IEvent{
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallListener> interesanci = MyEventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener listener : interesanci) {
            listener.callEnded(call_id);
        }
    }
}
