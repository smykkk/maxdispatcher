import interfaces.ICallListener;
import interfaces.IEvent;
import javafx.event.EventDispatcher;

import java.util.List;

public class CallStartedEvent implements IEvent {
    private int call_id;

    public CallStartedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallListener> interesanci = MyEventDispatcher.instance.getAllObjectsImplementingInterface(ICallListener.class);
        for (ICallListener listener : interesanci) {
            listener.callStarted(call_id);
        }
    }
}
