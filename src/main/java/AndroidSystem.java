import com.cycling74.max.DataTypes;
import com.cycling74.max.MaxObject;
import interfaces.ICallListener;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter

public class AndroidSystem extends MaxObject implements ICallListener {

    private boolean isCalling;
    public int id;
    private List<Object> objectList = new ArrayList<>();


    private AndroidSystem() {
        MyEventDispatcher.getInstance().register(this);
        createInfoOutlet(true);
        declareInlets(new int[]{DataTypes.ANYTHING, DataTypes.INT});
        declareOutlets(new int[]{DataTypes.INT});
        declareAttribute("ID");
    }


    @Override
    protected void loadbang() {
        post("Welcome to the patch");
    }

    @Override
    protected void inlet(int value) {
        int inlet = getInlet();
        switch (inlet) {
            case 0: {
                if (!isCalling) {
                    isCalling = true;
                    connectCall(value);
                } else {
                    System.out.println("stop current call first!");
                    outlet(1, "stop current call first!");
                }
                break;
            }
            case 1: {
                if (isCalling) {
                    isCalling = false;
                    disconnectCall(value);
                } else {
                    System.out.println("no call to stop");
                    outlet(1, "no call to stop");
                }
                break;
            }
        }

    }

    @Override
    protected void bang() {
        outletBang(0);
    }

    @Override
    public void zap() {
        super.zap();
    }

    @Override
    public void callStarted(int call_id) {
            System.out.println("Android: call " + call_id + " started");
            outlet(1, "Android: call " + call_id + " started");

    }

    @Override
    public void callEnded(int call_id) {
            System.out.println("Android: call " + call_id + " ended");
            outlet(1, "Android: call " + call_id + " ended");
    }

    public void connectCall(int call_id){
        MyEventDispatcher.getInstance().fireEvent(new CallStartedEvent(call_id));
    }

    public void disconnectCall(int call_id){
        MyEventDispatcher.getInstance().fireEvent(new CallEndedEvent(call_id));
    }
}
