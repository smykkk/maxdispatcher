
import com.cycling74.max.MaxObject;
import com.sun.istack.internal.NotNull;
import interfaces.IEvent;
import org.apache.commons.lang3.ClassUtils;

import java.util.*;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by amen on 12/4/17.
 */
public class MyEventDispatcher extends MaxObject{
    public static final MyEventDispatcher instance = new MyEventDispatcher();

    private Map<Class<?>, List<Object>> map = new HashMap<>();
    //    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    private ExecutorService executorService = Executors.newFixedThreadPool(3);

    public void register(Object o) {
        List<Class<?>> interfaces = ClassUtils.getAllInterfaces(o.getClass());
        for (Class<?> iinterface : interfaces) {
            // wszystkie obiekty ktore implementuja ten interfejs
            List<Object> objects = map.get(iinterface);

            if (objects == null) {
                objects = new ArrayList<>();
            }

            objects.add(o);
            map.put(iinterface, objects);
        }
    }

    public <T> List<T> getAllObjectsImplementingInterface(Class<T> clas) {
        List<T> lista = (List<T>) map.get(clas);
//        System.out.println("Szukam obiektów implementujących interfejs: " + clas.getName() + " znalezione obiekty: ");
//        for (T t : lista) {
//            System.out.println(" ----> " + t);
//        }
//        System.out.println();

        return (List<T>) map.get(clas);
    }

    public void fireEvent(@NotNull IEvent event) {

        // bad
//        try {
//            executorService.submit(event);
//        }catch (Exception e){
//            System.out.println(e.getMessage());
//        }

        //ok 1
        executorService.submit(() -> {
            try {
                event.run();
            } catch (Exception e) {
                e.printStackTrace();
                System.err.println(e.getMessage());
            }
        });

//        executorService.submit(new Runnable() {
//            @Override
//            public void run() {
//                try{
//                    event.run();
//                }catch (Exception e){
//                    e.printStackTrace();
//                    System.err.println(e.getMessage());
//                }
//            }
//        });
    }

    private MyEventDispatcher() {
    }

    public static MyEventDispatcher getInstance() {
        return instance;
    }
}