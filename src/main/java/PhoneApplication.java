import interfaces.ICallListener;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class PhoneApplication extends Application implements ICallListener{

    CallEntry currentEntry = new CallEntry();
    List<CallEntry> callRegistry = new LinkedList<>();

    public PhoneApplication() {
        MyEventDispatcher.getInstance().register(this);
    }

    @Override
    public void callStarted(int call_id) {
        currentEntry = new CallEntry();
        currentEntry.setCall_id(call_id);
        currentEntry.setStart(LocalDateTime.now());
        System.out.println("PhoneApplication: call " + call_id + " started");
        outlet(0, 0);
        outlet(1, "call " + call_id + " started");
    }

    @Override
    public void callEnded(int call_id) {
        currentEntry.setFinish(LocalDateTime.now());
        callRegistry.add(currentEntry);
        System.out.println("PhoneApplication: call ended");
        outlet(0, 1);
        outlet(1, "call ended");

    }

    public void displayRegistry(){
        for (CallEntry callEntry: callRegistry) {
            error(callEntry.toString());
        }
    }

    public class CallEntry{
        private int call_id;
        private LocalDateTime start;
        private LocalDateTime finish;

        public int getCall_id() {
            return call_id;
        }

        public void setCall_id(int call_id) {
            this.call_id = call_id;
        }

        public LocalDateTime getStart() {
            return start;
        }

        public void setStart(LocalDateTime start) {
            this.start = start;
        }

        public LocalDateTime getFinish() {
            return finish;
        }

        public void setFinish(LocalDateTime finish) {
            this.finish = finish;
        }

        @Override
        public String toString() {
            return "\n{" +
                    "call_id=" + call_id +
                    "\n start=" + start +
                    "\n finish=" + finish +
                    '}';
        }
    }
}
