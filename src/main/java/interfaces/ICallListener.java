package interfaces;

public interface ICallListener {
    void callStarted(int call_id);
    void callEnded(int call_id);

}
