package interfaces;

/**
 * Created by amen on 12/4/17.
 */
public interface IEvent extends Runnable{
    void run();
}
