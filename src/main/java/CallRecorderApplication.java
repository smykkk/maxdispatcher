import com.cycling74.max.DataTypes;
import com.cycling74.max.MaxObject;
import interfaces.ICallListener;

public class CallRecorderApplication extends Application implements ICallListener {

    public CallRecorderApplication() {
        MyEventDispatcher.getInstance().register(this);
    }

    @Override
    public void callStarted(int call_id) {
        System.out.println("CallRecorderApplication: call " + call_id + " started");
        outlet(0, 1);
        outlet(1, "recording call " + call_id);
    }

    @Override
    public void callEnded(int call_id) {
        System.out.println("CallRecorderApplication: call ended");
        outlet(0, 0);
        outlet(1, "call recorded");

    }
}
